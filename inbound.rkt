#lang typed/racket/no-check

(provide:
 [tws-inbound-msg (-> ClientId Msg Void)])

(require
 typed/racket/unit
 (only-in "comm/types.rkt"
          Msg message) 
 (only-in "api/rsp/tws-ready-sig.rkt"
          tws-ready^)
 (only-in "api/rsp/tws-inbound-sig.rkt"
          tws-inbound^)
 (only-in "ready-unit.rkt"
          tws-ready-syndicate@)
 (only-in "api/rsp/tws-inbound-dispatch-sig.rkt"
          tws-inbound-dispatch^)
 (only-in "api/rsp/tws-inbound-handler-unit.rkt"
          tws-inbound-handler@)
  (only-in "api/rsp/dispatch-ground-unit.rkt"
          tws-dispatch-syndicate@))

(define-values/invoke-unit tws-ready-syndicate@
  (import)
  (export tws-ready^))

(define-values/invoke-unit tws-dispatch-syndicate@
  (import tws-ready^)
  (export tws-inbound-dispatch^))

(: tws-inbound-msg (-> Msg Void))
(define-values/invoke-unit tws-inbound-handler@
  (import tws-inbound-dispatch^ tws-ready^)
  (export tws-inbound^))
