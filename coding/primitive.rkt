#lang typed/racket/no-check

(provide
 tws-ks-decode
 tws-string
 tws-symbol
 tws-float tws-float-default tws-float-option
 tws-integer tws-integer-option
 tws-nonneg-integer
 tws-boolean)

(require
 (only-in "parse.rkt"
          next-field)
 (only-in "decode.rkt"
          decode-integer
          decode-float
          decode-float-default
          decode-bool)
 bitsyntax)

#|
Tws encodes everything as as sequence of null terminated string fields.
Further decoding involves then converting this string value to specific field types, e.g. booleans, floats, integers, symbol enumerations involve converting strings to the
appropriate types.

Bitsyntax uses a CPS style.  After successfully parsing a segment (the next msg field in tws) of the byte stream the provided success continuation function is
invoked with parsed value and the remainder of the input byte stream.

This macro leverages tws-string to obtain the string field and then applies the provided conversion function to the string to obtain the
desired type value.  e.g. "1" -> #t, "0" -> #f for a tws string encoded boolean field.

After conversion the bitsyntax success continuation is invoked to continue parsing the next field value.  If the conversion fails the bitsyntax
failure continuation is invoked.  The invoked conversion functinon indicates failure to convert by returning #f, wherein this tws-ks-decode macro
will invoke the provided failure continuation (kf).

In the bitsyntax world, invoking the failure continuation sematics are "this particular bitsyntax parsing match pattern failed to match the input".
Also, an encoder has #f as the first parameter, decoders #t.  See Racket's bitsyntax documentation.

|#
(define-syntax tws-ks-decode
  (syntax-rules ()
    [(_ bstr decode-fn ks kf)
     (let ((ks-decode (λ ((str : String) (rst : BitString))
                        (let ((x (decode-fn str)))
                          (if x
                              (ks x rst) ;; success in parsing the field to the desired type, continue parsing the next field.
                              (kf))))))  ;; failed to parse the tws field to the desired type, fail parsing, i.e. the bit-syntax-case match pattern failed.
       (tws-string #t bstr ks-decode kf))]))


(define-syntax tws-ks-decode-option
  (syntax-rules ()
    [(_ bstr decode-fn ks kf)
     (let ((ks-decode (λ ((str : String) (rst : BitString))
                        (let ((x (decode-fn str)))
                          (ks x rst)))))
       (tws-string #t bstr ks-decode kf))]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; The basic primitive field types. ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


#|
Bitsyntax works with bytes. A tws msg is a stream of null terminated string fields.

The macro is a bitsyntax encoder/decoder for a base tws string field.
As all other types ultimately are encoded/decoded as strings all other encoder/decoders
are built on top of this macro.
|#
(define-syntax tws-string
  (syntax-rules ()
    [(_ #f val)
     (let ((s-val (string->bytes/utf-8 (format "~a" val))))
       (bit-string (s-val :: binary) 0))]
    [(_ #t bstr ks kf)
     (let-values (([s t] (next-field bstr)))
       ;;(displayln (format "S: ~v" s))
       (ks s t))]))

(module+ test
  (require typed/rackunit)
  (require bitsyntax)

  (check-equal? "hello"
                (bit-string-case #"hello\0"
                                 (([h :: (tws-string)])
                                  h)))

  (let-values (([a b c] (bit-string-case #"a\0b\0c\0"
                                         (([a :: (tws-string)]
                                           [b :: (tws-string)]
                                           [c :: (tws-string)])
                                          (values a b c)))))
    (check-equal? "a" a)
    (check-equal? "b" b)
    (check-equal? "c" c)))

#|
;; 'Manual' example of encode/decode of a float using tws-string bitsyntax macro.
;; The actual tws-float decoding uses the tws-ks-decode macro to remove
;; the boilerplate shown here.
;; i.e tws-float using tws-ks-decode expands equivalently to the code shown here.

(define-syntax tws-float
  (syntax-rules ()
    [(_ #f x)
     (let ((f : Float x))
       (tws-string #f (number->string f)))]
    [(_ #t bstr ks kf)
     (let ((ks-float (λ ((str : String) (rst : BitString))
                       (let ((f : Float (decode-float str)))
                         (if f
                             (ks f rst)
                             (kf))))))
       (tws-string #t bstr ks-float kf))]))
|#


#| Encode/Decode a float field for tws message |#
(define-syntax tws-float
  (syntax-rules ()
    [(_ #f x)
     (let ((f : Float x))
       (tws-string #f (number->string f)))]
    [(_ #t bstr ks kf)
     (tws-ks-decode bstr decode-float ks kf)]))

(define-syntax tws-float-option
  (syntax-rules ()
    [(_ #t bstr ks kf)
     (tws-ks-decode-option bstr decode-float ks kf)]))

(define-syntax tws-float-default
  (syntax-rules ()
    [(_ #f x)
     (tws-float #f x)]
    [(_ #t bstr ks kf)
     (tws-ks-decode bstr decode-float-default ks kf)]))

(module+ test
  (check-equal?
   (bit-string-case #"1.0\0"
                    (([f :: (tws-float)])
                     f))
   1.0))

#| Encode/Decode an integer field for a tws message |#
(define-syntax tws-integer
  (syntax-rules ()
    [(_ #f x)
     (let ((i : Integer x))
       (tws-string #f (number->string i)))]
    [(_ #t bstr ks kf)
     (tws-ks-decode bstr decode-integer ks kf)]))

(define-syntax tws-integer-option
  (syntax-rules ()
    [(_ #t bstr ks kf)
     (tws-ks-decode-option bstr decode-integer ks kf)]))


(module+ test
  (check-equal? (bit-string-case #"1\0"
                                 (([t :: (tws-integer)])
                                  t))
                1))

(define-syntax tws-nonneg-integer
  (syntax-rules ()
    [(_ #f x)
     (let ((p : Integer x))
       (tws-string #f (number->string i)))]
    [(_ #t bstr ks kf)
     (let ((decode-nn-int (λ ((s : String))
                            (let ((n (string->number s)))
                              (if (exact-nonnegative-integer? n)
                                  n
                                  #f)))))
       (tws-ks-decode bstr decode-nn-int ks kf))]))

#|
Encode/Decode a symbol field.
Note: A Racket symbol, not a stock symbol. :)
|#
(define-syntax tws-symbol
  (syntax-rules ()
    [(_ #f sym)
     (tws-string #f (symbol->string sym))]
    [(_ #t bstr ks kf)
     (let ((decode-symbol (λ ((str : String))
                            (string->symbol str))))
       (tws-ks-decode bstr decode-symbol ks kf))]))


#| Encode/Decode a boolean value. |#
(define-syntax tws-boolean
  (syntax-rules ()
    [(_ #f b)
     (if b
         (tws-string #f "1")
         (tws-string #f "0"))]
    [(_ #t bstr ks kf)
     (let ((ks-loc (lambda ((str : String) (rst : BitString))
                     (let ([b (decode-bool str)])
                       (if (boolean? b)
                           (ks b rst)
                           (kf))))))
       (tws-string #t bstr ks-loc kf))]))

(module+ test
  
  (check-equal? (bit-string-case #"1\0"
                                 (([t :: (tws-boolean)])
                                  t))
                #t)

  (check-equal? (bit-string-case #"1.0\0"
                                 (([f :: (tws-float)])
                                  f))
                1.0)


  (check-equal? (bit-string-case #"\0"
                                 (([f :: (tws-float-default)])
                                  f))
                0.0)

  (check-equal? (bit-string-case #"\0"
                                 (([f :: (tws-float-option)])
                                  f))
                #f)
  

  (check-equal? (bit-string-case #"1.0\0"
                                 (([f :: (tws-float-option)])
                                  f))
                1.0)

  
  )



