#lang typed/racket/no-check

(provide
 tws-account-list
 tws-action
 tws-api-client-id
 tws-api-order-id
 tws-bars
 tws-bar-size
 tws-client-id
 tws-currency
 tws-date
 tws-date-format
 tws-time
 tws-duration
 tws-exchange
 tws-family-codes
 tws-generic-ticks
 tws-histogram-entries
 tws-historical-ticks
 tws-historical-ticks-bid-ask
 tws-historical-ticks-last
 tws-historical-type
 tws-listof tws-vectorof tws-setof
 tws-market-datatype
 tws-market-depth-descriptions
 tws-message-code
 tws-msg-id
 tws-news-providers
 tws-order-id
 tws-price
 tws-price-increments
 tws-request-id
 tws-security-symbol
 tws-security-type
 tws-security-type-optional
 tws-smart-component-map
 tws-snapshot
 tws-soft-dollar-tiers
 tws-special-conditions
 tws-ticker-icmd
 tws-ticker-id)

(require
 bitsyntax
 (only-in "parse.rkt"
          next-field
          next-integer)
 (only-in "primitive.rkt"
          tws-ks-decode
          tws-symbol tws-string
          tws-boolean tws-float
          tws-integer tws-nonneg-integer)
 (only-in "../data/bar-size.rkt"
          Bar-Size Bar-Second Bar-Minute Bar-Hour Bar-Day Bar-Week Bar-Month)
 (only-in "../data/field.rkt"
          Action
          api-client-id ;; t-ctor
          api-order-id
          Bar
          Currency currency
          DepthMktDataDescription
          Duration Duration-unit Duration-value Duration-Unit
          ExchangeEntry
          FamilyCode
          HistogramEntry
          Historical-Type
          HistoricalTick
          HistoricalTickBidAsk
          HistoricalTickLast
          SmartComponentMap
          Time time
          MsgId
          RequestId request-id
          Market-Datatype
          NewsProvider
          order-id ;; ctor
          PriceIncrement
          Price price
          SoftDollarTier
          SecurityType security-type?
          Snapshot
          TickerId ticker-id
          GenericTick)
 (only-in "../data/tick.rkt"
          TickType tick-type?)
 (only-in "../data/tick-attribute.rkt"
          tick-attribute-last-from-mask
          tick-attribute-bid-ask-from-mask))


(define-syntax tws-listof
  (syntax-rules ()
    ([_ #t bstr ks kf rtn-t rd-fn]
     (let ((rs (bit-string-case bstr
                                (([cnt  :: (tws-integer)]
                                  [rest0 :: binary])
                                 (let loop : (List (Listof rtn-t) BitString Boolean)
                                      ([cnt cnt] [rest rest0] (xs : (Listof rtn-t) '()))
                                      (if (zero? cnt)
                                          (list (reverse xs) rest #t)
                                          (let-values ([(x bs) (bit-string-case rest
                                                                                (([x    :: (rd-fn)]
                                                                                  [rest :: binary])
                                                                                 (values x rest)))])
                                            (loop (sub1 cnt) bs (cons x xs))))))
                                (([no-cnt :: (tws-string)]
                                  [rest1 :: binary])
                                 (let ((l : (Listof rtn-t) '()))
                                   (displayln "LO Empty")
                                   (if (equal? no-cnt "")
                                       (list l rest1 #t)
                                       (list l rest1 #f)))))))
       (if (caddr rs)
           (ks (car rs) (cadr rs))
           (kf))))))


#| Read off N homogeneous elements into a vector. |#
(define-syntax tws-vectorof
  (syntax-rules ()
    ([_ #t bstr ks kf e-t e-rd]
     (let ((ks-v (λ ((es : (Listof e-t)) (rest : BitString))
                   (let ((vs : (Vectorof e-t) (apply vector es)))
                     (ks vs rest)))))
       (tws-listof #t bstr ks-v kf e-t e-rd)))))

(define-syntax tws-setof
  (syntax-rules ()
    ([_ #t bstr ks kf e-t e-rd]
     (let ((ks-s (λ ((es : (Listof e-t)) (rest : BitString))
                   (let ((ss : (Setof e-t) (apply set es)))
                     (ks ss rest)))))
       (tws-listof #t bstr ks-s kf e-t e-rd)))))


(define-syntax tws-order-id
  (syntax-rules ()
    [(_ #t bstr ks kf)
     (let ((decode-oid (λ ((s : String))
                         (let ((n (string->number s)))
                           (if (exact-nonnegative-integer? n)
                               (order-id n)
                               #f)))))
       (tws-ks-decode bstr decode-oid ks kf))]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; TWS messsage level primitive field values. ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#| TWS Snapshot
Ultimately encoded as either "1" or "0", which is tws's coding of a boolean flag value.
|#
(define-syntax tws-snapshot
  (syntax-rules ()
    [(_ #f v)
     (let ([v1 : Snapshot v])
       (if (symbol=? v1 'Snapshot)
           (tws-boolean #f #t)
           (tws-boolean #f #f)))]))

#| Encode a unique client request identier, a unique (to a clients session) nonnegative integer value. |#
(define-syntax tws-msg-id
  (syntax-rules ()
    [(_ #f id)
     (let ([mid : MsgId id])
       (tws-integer #f mid))]))

(define-syntax tws-message-code
  (syntax-rules ()
    [(_ #t bstr ks kf)
     (bit-string-case bstr
                      (([mid  :: (tws-nonneg-integer)]
                        [rest :: binary])
                       (ks mid rest)))]))

(: decode-request-id (-> String (Option RequestId)))
(define (decode-request-id s)
  (let ((n (string->number s)))
    (if (exact-integer? n)
        (request-id n)
        #f)))

(define-syntax tws-request-id
  (syntax-rules ()
    [(_ #f id)
     (let ((rid : RequestId id))
       (tws-integer #f rid))]
    [(_ #t bs ks kf)
     (tws-ks-decode bs decode-request-id ks kf)]))

#| FIXME RPR - Currently I believe this is the message id and tws doc is just inconsistent in its naming. |#

(: decode-ticker-id (-> String (Option TickerId)))
(define (decode-ticker-id s)
  (let ((tid (string->number s)))
    (if (exact-nonnegative-integer? tid)
        (ticker-id tid)
        #f)))

(define-syntax tws-ticker-id
  (syntax-rules ()
    ([_ #f id]
     (let ([tid : TickerId id])
       (tws-integer #f tid)))
    ([_ #t bstr ks kf]
     (tws-ks-decode bstr decode-ticker-id ks kf))))

#| Encodes the type of a security's identifer, e.g. cusip |#
(define-syntax tws-security-type
  (syntax-rules ()
    ([_ #f id]
     (let ([st : SecurityType id])
       (tws-symbol #f st)))
    ([_ #t bitstr ks kf]
     (let ((sec-type (λ ((s : Symbol) (rest : BitString))
                       (if (security-type? s)
                           (ks s rest)
                           (kf)))))
       (tws-symbol #t bitstr sec-type kf)))))

(define-syntax tws-security-type-optional
  (syntax-rules ()
    ([_ #f id]
     (let ([st : SecurityType id])
       (tws-symbol #f st)))
    ([_ #t bitstr ks kf]
     (let ((sec-type (λ ((s : Symbol) (rest : BitString))
                       (cond
                         ((security-type? s)
                          (ks s rest))
                         ((equal? s '||)
                          (ks #f rest))
                         (kf)))))
       (tws-symbol #t bitstr sec-type kf)))))

#| Encodes a security's symbol, e.g. "AMZN" |#
(define-syntax tws-security-symbol
  (syntax-rules ()
    [(_ #f s)
     (tws-string #f s)]))

#| Encodes a Tws formatted date string. |#
(define-syntax tws-date
  (syntax-rules ()
    [(_ #f d)
     (tws-string #f d)]
    [(_ #t bstr ks kf)
     (tws-string #t bstr ks kf)]))

(: decode-currency (-> String Currency))
(define (decode-currency s)
  (currency s))

#| Encodes a Tws currencty string value, e.g. "USD" |#
(define-syntax tws-currency
  (syntax-rules ()
    ([_ #t bitstr ks kf]
     (tws-ks-decode bitstr decode-currency ks kf))
    [(_ #f c)
     (tws-string #f c)]))

(define-syntax tws-api-client-id
  (syntax-rules ()
    ([_ #t bstr ks kf]
     (tws-ks-decode bstr (λ ([s : String])
                           (let ((n (string->number s)))
                             (if (exact-nonnegative-integer? n)
                                 (api-client-id n)
                                 #f)))
                    ks kf))))

(define-syntax tws-api-order-id
  (syntax-rules ()
    ([_ #t bstr ks kf]
     (tws-ks-decode bstr (λ ([s : String])
                           (let ((n (string->number s)))
                             (if (exact-nonnegative-integer? n)
                                 (api-order-id n)
                                 #f)))
                    ks kf))))

(define-syntax tws-ticker-icmd
  (syntax-rules ()
    ([_ #t bstr ks kf]
     (tws-symbol #t bstr ks kf))))

(: decode-action (-> String (Option Action)))
(define (decode-action s)
  (cond
    ((equal? s "Buy") 'Buy)
    ((equal? s "Sell") 'Sell)
    ((equal? s "SShort") 'SShort)
    (else #f)))

#| Encode the desired contract action.
e.g.  Buy or sell a stock, bond or option.
|#
(define-syntax tws-action
  (syntax-rules ()
    [( _ #f a)
     (tws-symbol #f a)]
    [(_ #t bstr ks kf)
     (tws-ks-decode bstr decode-action ks kf)]))

#| Encodes the type tag of data values that may be associated with a tick of a security.
These are encoded in tws messages as some integer value.
e.g. The market price float value is "tagged" with an integer value prefix field of 221.
'... "221" null "12.34" null ...' would deocode as the tics market price is $12.34
|#

(: encode-generic-tick (-> GenericTick Exact-Nonnegative-Integer))
(define (encode-generic-tick gtic)
  (case gtic
    ([Option-Volume] 108)
    ([Option-Open-Interest] 101)
    ([Historical-Volatility] 184)
    ([Average-Option-Volume] 185)
    ([Option-Implied-Volatility] 186)
    ([Index-Future-Premium] 162)
    ([Miscellaneous] 165)
    ([Market-Price] 221)
    ([Auction-Values] 225)
    ([RTVolume] 233)
    ([Shortable] 236)
    ([Inventory] 256)
    ([Fundamental-Ratios] 258)
    ([Real-time-Historical-Voltility] 311)
    ([IBDividends] 456)
    (else (error 'InvalidGenericTick (format "Unknown ~a." gtic)))))

#| A list of generic ticks is encoded as a comma separated string of code values. |#
(define-syntax tws-generic-ticks
  (syntax-rules ()
    [(_ #f tics)
     (let ((t-tics : (Listof GenericTick)) tics)
       (let ((n-tics : (Listof Exact-Nonnegative-Integer)  (map encode-generic-tick t-tics)))
         (tws-string #f (string-join n-tics ","))))]))

#| Encodes the type of market date request/recieved..
Encoded as an integer value.
e.g. Live data or delayed or frozen (after close) etc.
|#
(: encode-market-datatype (-> Market-Datatype Exact-Nonnegative-Integer))
(define (encode-market-datatype mdt)
  (case mdt
    [(Live)          1]
    [(Frozen)        2]
    [(Delayed)       3]
    [(FrozenDelayed) 4]))

(define-syntax tws-market-datatype
  (syntax-rules ()
    [(_ #f mdt)
     (let ([market-datatype : Market-Datatype mdt])
       (tws-integer #f (encode-market-datatype market-datatype)))]))

(: decode-tws-time (-> String (Option Time)))
(define (decode-tws-time s)
  (let ((n (string->number s)))
    (if (exact-integer? n)
        (time n)
        #f)))

#| Encode/Decode a tws time string |#
(define-syntax tws-time
  (syntax-rules ()
    ;;[(_ #f t) (tws-string #f t)]
    [(_ #t bstr ks kf) (tws-ks-decode bstr decode-tws-time ks kf)]))


(: encode-duration-unit (-> Duration-Unit String))
(define (encode-duration-unit dur)
  (case dur
    [(Second) "S"]
    [(Day)    "D"]
    [(Week)   "W"]
    [(Month)  "M"]
    [(Year)   "Y"]
    [else (error 'EncodeDurationUnit)])) ;; can't happen

(define-syntax tws-duration
  (syntax-rules ()
    [(_ #f dur)
     (let ((u (encode-duration-unit (Duration-unit dur)))
           (n (number->string (Duration-value dur))))
       (tws-string #f (string-append n " " u)))]))

(define-syntax tws-date-format
  (syntax-rules ()
    [(_ #f df)
     (case df
       ([YMD] (tws-integer #f 1))
       ([Seconds] (tws-integer #f 2)))]))

(: encode-historical-type (-> Historical-Type String))
(define (encode-historical-type ht)
  (regexp-replace #rx"_" (symbol->string ht) "-"))

(define-syntax tws-historical-type
  (syntax-rules ()
    [( _ #f ht)
     (tws-string #f (encode-historical-type ht))]))

(: bar-units (-> Integer String String))
(define (bar-units v u)
  (if (= v 1)
      u
      (string-append u "s")))


(define (encode-bar-size bs)
  (match bs
    ((Bar-Day)   "1 day")
    ((Bar-Week)  "1 week")
    ((Bar-Month) "1 month")
    ((Bar-Hour hrs)
     (string-append (number->string hrs) " " (bar-units hrs "hour")))
    ((Bar-Minute mins)
     (string-append (number->string mins) " " (bar-units mins "min")))
    ((Bar-Second secs)
     (string-append (number->string secs) " " "secs"))))

(define-syntax tws-bar-size
  (syntax-rules ()
    [(_ #f bs)
     (tws-string #f (encode-bar-size bs))]))


(define-syntax tws-price
  (syntax-rules ()
    [(_ #t bstr ks kf)
     (bit-string-case bstr
                      (([p    :: (tws-float)]
                        [rest :: binary])
                       (ks (price p) rest)))]))

#| Encode/Decode a tws exchange field |#
(define-syntax tws-exchange
  (syntax-rules ()
    [(_ #f t)       (tws-string #f t)]
    [(_ #t t ks kf) (tws-string #t t ks kf)]))

#| Encode/Decode a tws special conditions qualifier field. |#
(define-syntax tws-special-conditions
  (syntax-rules ()
    [(_ #f t)       (tws-string #f t)]
    [(_ #t t ks kf) (tws-string #t t ks kf)]))

(define-syntax tws-client-id
  (syntax-rules ()
    [(_ #f cid)       (tws-string #f cid)]
    [(_ #t cid ks kf) (tws-string #t cid ks kf)]))

(define-syntax tws-account-list
  (syntax-rules ()
    [(_ #t bstr ks kf)
     (tws-listof #t bstr ks kf String tws-string)]))

(define-syntax tws-soft-dollar-tier
  (syntax-rules ()
    [(_ #t bstr ks kf)
     (bit-string-case bstr
                      (([name :: (tws-string)]
                        [val  :: (tws-string)]
                        [disp :: (tws-string)]
                        [rest :: binary])
                       (ks (SoftDollarTier name val disp) rest)))]))

(define-syntax tws-soft-dollar-tiers
  (syntax-rules ()
    [(_ #t bstr ks kf)
     (tws-vectorof #t bstr ks kf SoftDollarTier tws-soft-dollar-tier)]))

(define-syntax tws-family-code
  (syntax-rules ()
    ([_ #t bstr ks kf]
     (bit-string-case bstr
                      (([accnt :: (tws-string)]
                        [code  :: (tws-string)]
                        [rest  :: binary])
                       (ks (FamilyCode accnt code) rest))))))

(define-syntax tws-family-codes
  (syntax-rules ()
    [(_ #t bstr ks kf)
     (tws-vectorof #t bstr ks kf FamilyCode tws-family-code)]))

;; Boolean flag: #t -> Deep2, #f -> Deep
(define-syntax tws-service-data-type
  (syntax-rules ()
    ([_ #t bstr ks kf]
     (bit-string-case bstr
                      (([flg  :: (tws-boolean)]
                        [rest :: binary])
                       (let ((dt (if flg 'Deep2 'Deep)))
                         (ks dt rest)))))))

(define-syntax tws-market-depth-description
  (syntax-rules ()
    [(_ #t bstr ks kf)
     (bit-string-case bstr
                      (([exch   :: (tws-string)]
                        [sec-ty :: (tws-string)]
                        [s-dt   :: (tws-service-data-type)]
                        [rest   :: binary])
                       (ks (DepthMktDataDescription exch sec-ty "" s-dt 0) rest)))]))

(define-syntax tws-market-depth-descriptions
  (syntax-rules ()
    ([_ #t bstr ks kf]
     (tws-vectorof #t bstr ks kf DepthMktDataDescription tws-market-depth-description))))


(define-syntax tws-exchange-entry
  (syntax-rules ()
    ([_ #t bstr ks kf]
     (bit-string-case bstr
                      (([bn   :: (tws-integer)]
                        [exch :: (tws-string)]
                        [lttr :: (tws-string)]
                        [rest :: binary])
                       (ks (cons bn (ExchangeEntry exch lttr)) rest))))))

(define-syntax tws-smart-component-map
  (syntax-rules ()
    [(_ #t bstr ks kf)
     (bit-string-case bstr
                      (([rid   :: (tws-request-id)]
                        [ees   :: (tws-listof (Pairof Integer ExchangeEntry) tws-exchange-entry)]
                        [rest  :: binary])
                       (let ((scm : SmartComponentMap (make-immutable-hash ees)))
                         (ks scm rest))))]))

(define-syntax tws-news-provider
  (syntax-rules ()
    [(_ #t bstr ks kf)
     (bit-string-case bstr
                      (([code :: (tws-string)]
                        [name :: (tws-string)]
                        [rest :: binary])
                       (ks (NewsProvider code name) rest)))]))

(define-syntax tws-news-providers
  (syntax-rules ()
    [(_ #t bstr ks kf)
     (tws-vectorof #t bstr ks kf NewsProvider tws-news-provider)]))

(define-syntax tws-histogram-entry
  (syntax-rules ()
    ([_ #t bstr ks kf]
     (bit-string-case bstr
                      (([prc :: (tws-float)]
                        [sz  :: (tws-integer)]
                        [rest :: binary])
                       (ks (HistogramEntry prc sz) rest))))))

(define-syntax tws-histogram-entries
  (syntax-rules ()
    ([_ #t bstr ks kf]
     (tws-vectorof #t bstr ks kf HistogramEntry tws-histogram-entry))))

(define-syntax tws-price-increment
  (syntax-rules ()
    ([_ #t bstr ks kf]
     (bit-string-case bstr
                      (([le   :: (tws-float)]
                        [inc  :: (tws-float)]
                        [rest :: binary])
                       (ks (PriceIncrement le inc) rest))))))


(define-syntax tws-price-increments
  (syntax-rules ()
    ([_ #t bstr ks kf]
     (tws-vectorof #t bstr ks kf PriceIncrement tws-price-increment))))


(define-syntax tws-bar
  (syntax-rules ()
    ([_ #t bstr ks kf]
     (bit-string-case bstr
                      (([date :: (tws-string)]
                        [open :: (tws-float)]
                        [high :: (tws-float)]
                        [low  :: (tws-float)]
                        [close :: (tws-float)]
                        [vol   :: (tws-integer)]
                        [wap   :: (tws-float)]
                        [cnt   :: (tws-integer)]
                        [rest  :: binary])
                       (ks (Bar date open high low close vol cnt wap) rest))))))

(define-syntax tws-bars
  (syntax-rules ()
    ([_ #t bstr ks kf]
     (tws-listof #t bstr ks kf Bar tws-bar))))

(define-syntax tws-historical-tick
  (syntax-rules ()
    ([_ #t bstr ks kf]
     (bit-string-case bstr
                      (([time :: (tws-time)]
                        [skip :: (tws-integer)] ;; for consistency??
                        [prc  :: (tws-price)]
                        [size :: (tws-nonneg-integer)]
                        [rest :: binary])
                       (ks (HistoricalTick time prc size) rest))))))


(define-syntax tws-historical-ticks
  (syntax-rules ()
    ([_ #t bstr ks kf]
     (tws-vectorof #t bstr ks kf HistoricalTick tws-historical-tick))))

(define-syntax tws-historical-tick-bid-ask
  (syntax-rules ()
    ([_ #t bstr ks kf]
     (bit-string-case rest
                      (([time   :: (tws-time)]
                        [bm     :: (tws-integer)]
                        [bid    :: (tws-price)]
                        [ask    :: (tws-price)]
                        [bid-sz :: (tws-nonneg-integer)]
                        [ask-sz :: (tws-nonneg-integer)]
                        [rest   :: binary])
                       (let ((attr (tick-attribute-bid-ask-from-mask bm)))
                         (ks (HistoricalTickBidAsk time attr bid ask bid-sz ask-sz) rest)))))))

(define-syntax tws-historical-ticks-bid-ask
  (syntax-rules ()
    [(_ #t bstr ks kf)
     (bit-string-case bstr
                      (([htba :: (tws-vectorof HistoricalTickBidAsk tws-historical-tick-bid-ask)]
                        [rest :: binary])
                       (ks htba rest)))]))

(define-syntax tws-historical-tick-last
  (syntax-rules ()
    [(_ #t bstr ks kf)
     (bit-string-case bstr
                      (([time :: (tws-time)]
                        [bm   :: (tws-integer)]
                        [prc  :: (tws-price)]
                        [sz   :: (tws-nonneg-integer)]
                        [exch :: (tws-string)]
                        [spc  :: (tws-string)]
                        [rest :: binary])
                       (ks (HistoricalTickLast time
                                               (tick-attribute-last-from-mask bm)                                               prc sz exch spc)
                           rest)))]))

(define-syntax tws-historical-ticks-last
  (syntax-rules ()
    [(_ #t bstr ks kf)
     (tws-vectorof #t bstr ks kf HistoricalTickLast tws-historical-tick-last)]))

(module+ test
  (require
   bitsyntax
   rackunit)

  (check-equal? (bit-string-case #"3\0a\0b\0c\0"
                                 (([accnts :: (tws-account-list)])
                                  accnts))
                (list "a" "b" "c")))

(module+ test

  (check-equal? (list "a" "b" "c")
                (bit-string-case #"3\0a\0b\0c\0hello\0"
                                 (([ss :: (tws-listof String tws-string)]
                                   [s  :: (tws-string)])
                                  ss)))

  (check-equal? (vector "a" "b" "c")
                (bit-string-case #"3\0a\0b\0c\0hello\0"
                                 (([sv :: (tws-vectorof String tws-string)]
                                   [s  :: (tws-string)])
                                  sv))))


(module+ test

  (check-equal? (list "ABC" 'OPT 1 "hello" "20200918" 260.0 "there")
                (bit-string-case #"ABC\0OPT\0001\0hello\00020200918\000260\0there\0"
                                 (([abc :: (tws-string)]
                                   [st :: (tws-security-type)]
                                   [one :: (tws-integer)]
                                   [hi  :: (tws-string)]
                                   [dt  :: (tws-string)]
                                   [sk  :: (tws-float)]
                                   [th  :: (tws-string)])
                                  (list abc st one hi dt sk th))))

  )

(module* field-test #f

  (require
   (only-in "primitive.rkt"
            tws-ks-decode
            tws-symbol tws-string
            tws-boolean tws-float
            tws-integer tws-nonneg-integer)         
   rackunit
   bitsyntax)

  
(test-begin "Security Type"
            (let ((st (bit-string-case #"STK\0"
                                       (([st :: (tws-security-type)])
                                        st))))
              (check-equal? st "STK")))

)
