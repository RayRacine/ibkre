#lang typed/racket/no-check

(provide
 decode-integer
 decode-float decode-float-default
 decode-bool)

(: decode-integer (-> String (Option Integer)))
(define (decode-integer s)
  (let ((n (string->number s)))
    (if (exact-integer? n) n #f)))

(: MAX-FLOAT Float)
(define MAX-FLOAT 1E308)

(: decode-float-default? (-> String Boolean (Option Float)))
(define (decode-float-default? str def?)
  (: mx (-> Float (Option Float)))
  (define (mx n)
    ;;(displayln (format "Max testing: ~a" n))
    (if (>= n MAX-FLOAT) n n))
  (if (and (equal? str "") def?)
      0.0
      (let ((n (string->number str)))
        (if (number? n)
            (cond
              ((flonum? n) (mx n))
              ((exact? n)                    ;; Racket Bug? (exact? #f) should be total
               (let ((r (exact->inexact n))) ;; (U Inexact-Complex Inexact-Real)
                 (cond
                   ((flonum? r) (mx r))      ;; no complex, single-flonum-available #f
                   (else #f))))
              (else #f))            
            #f))))

(: decode-float (-> String (Option Float)))
(define (decode-float str)
  (decode-float-default? str #f))

(: decode-float-default (-> String (Option Float)))
(define (decode-float-default str)
  (decode-float-default? str #t))

(: decode-bool (-> String (U Boolean Void)))
(define (decode-bool str)
  (cond
    ([string=? str "1"] #t)
    ([string=? str "0"] #f)
    (else (void))))
