#lang typed/racket/no-check

#| The general approach is to have outbound msgs generated from functions which return Bytes that is sent out via a TwsSend msg. |#

(provide
 contract-details-msg
 current-time-req
 executions-msg
 historical-bar-data
 cancel-historical-bar-data
 matching-symbols-msg
 mkt-data-msg
 mkt-data-cancel-msg
 market-datatype-msg
 sec-def-opt-params-msg
 tick-by-tick-data)

(require
 bitsyntax
 (only-in "../../comm/types.rkt"
          Msg message)
 (only-in "../../coding/msg.rkt"
          tws-msg
          tws-msg->msg)
 (only-in "../../data/msgids.rkt"
          CANCEL-HISTORICAL-DATA
          REQ-HISTORICAL-DATA
          REQ-CONTRACT-DATA
          REQ-MKT-DATA
          REQ-MARKET-DATATYPE
          REQ-MATCHING-SYMBOLS
          REQ-SEC-DEF-OPT-PARAMS
          REQ-TICK-BY-TICK-DATA
          CANCEL-MKT-DATA
          REQ-CURRENT-TIME)
 (only-in "../../coding/primitive.rkt"
          tws-integer)
 (only-in "../../coding/field.rkt"
          tws-bar-size
          tws-date
          tws-date-format
          tws-duration
          tws-historical-type
          tws-request-id
          tws-msg-id tws-ticker-id tws-security-type
          tws-snapshot tws-generic-ticks tws-market-datatype)
 (only-in "../../coding/primitive.rkt"
          tws-string tws-integer tws-float tws-boolean tws-symbol)
 (only-in "../../data/bar-size.rkt"
          Bar-Size Bar-Second Bar-Minute Bar-Hour
          Bar-Day Bar-Week Bar-Month)
 (only-in "../../data/field.rkt"
          Date Date-Format
          Duration
          ExecutionFilter
          Historical-Type
          RequestId request-id
          MsgId msg-id
          TickerId ticker-id
          SecurityType
          Snapshot
          GenericTick
          Market-Datatype)
 (only-in "../../data/tick.rkt"
          TickByTickType)
 (only-in "../../coding/tick.rkt"
          tws-tick-by-tick-type)
 (only-in "../../coding/contract.rkt"
          tws-contract)
 (only-in "../../data/contract.rkt"
          Contract
          Contract-conId Contract-symbol
          Contract-secType Contract-lastTradeDateOrContractMonth
          Contract-strike Contract-right Contract-multiplier
          Contract-exchange Contract-primaryExchange
          Contract-currency Contract-localSymbol Contract-tradingClass
          Contract-includeExpired Contract-secIdType Contract-secId))

(: current-time-req (-> Msg))
(define (current-time-req)
  (define VERSION "1")
  (tws-msg->msg (tws-msg REQ-CURRENT-TIME VERSION)))

(: market-datatype-msg (-> Market-Datatype Msg))
(define (market-datatype-msg mdt)
  (define VERSION 1)
  (message (bit-string->bytes (bit-string
                               ((msg-id REQ-MARKET-DATATYPE) :: (tws-msg-id))
                               (VERSION :: (tws-string))
                               (mdt     :: (tws-market-datatype))))))

#|
Validation Rules:
- Do not enter any generic-tick-list values if you use snapshots.
|#
(: mkt-data-msg (-> TickerId Contract String Snapshot Snapshot Msg))
(define (mkt-data-msg tic-id contract generic-tick-list snapshot regulatory-snapshot)
  (define INTERNAL-USE-ONLY-OPT-MKT-DATA "")
  (define VERSION "11")

  (define hdr (bit-string ((msg-id REQ-MKT-DATA)  :: (tws-msg-id))
                          (VERSION                :: (tws-string))
                          ((ticker-id tic-id)     :: (tws-ticker-id))))

  (define con  (bit-string (contract :: (tws-contract))))

  (define tail (bit-string
                ;; (generic-tick-list :: (tws-string))
                (generic-tick-list   :: (tws-string)) ;; FIXME RPR
                (snapshot            :: (tws-snapshot))
                (regulatory-snapshot :: (tws-snapshot))
                (INTERNAL-USE-ONLY-OPT-MKT-DATA :: (tws-string))))

  (message (bit-string->bytes (bit-string-append hdr con tail))))


(: mkt-data-cancel-msg (-> TickerId Msg))
(define (mkt-data-cancel-msg tic-id)
  (define VERSION 2)
  (message (bit-string->bytes
            (bit-string ((msg-id CANCEL-MKT-DATA) :: (tws-msg-id))
                        (VERSION                  :: (tws-string))
                        ((ticker-id tic-id)       :: (tws-ticker-id))))))

(: sec-def-opt-params-msg (-> RequestId Symbol String SecurityType Integer Msg))
(define (sec-def-opt-params-msg rid sym exch sec-t cid)
  (message (bit-string->bytes
            (bit-string
             ((msg-id REQ-SEC-DEF-OPT-PARAMS) :: (tws-msg-id))
             (rid    :: (tws-request-id))
             (sym    :: (tws-symbol))
             (exch   :: (tws-string))
             (sec-t  :: (tws-security-type))
             (cid    :: (tws-integer))))))


(: contract-details-msg (-> RequestId Contract Msg))
(define (contract-details-msg rid con)
  (define VERSION 8)
  (message (bit-string->bytes
            (bit-string
             ((msg-id REQ-CONTRACT-DATA)     :: (tws-msg-id))
             (VERSION                        :: (tws-integer))
             (rid                            :: (tws-request-id))
             ((Contract-conId           con) :: (tws-integer))
             ((Contract-symbol          con) :: (tws-string))
             ((Contract-secType         con) :: (tws-security-type))
             ((Contract-lastTradeDateOrContractMonth con) :: (tws-string))
             ((Contract-strike          con) :: (tws-float))
             ((Contract-right           con) :: (tws-string))
             ((Contract-multiplier      con) :: (tws-string))
             ((Contract-exchange        con) :: (tws-string))
             ((Contract-primaryExchange con) :: (tws-string))
             ((Contract-currency        con) :: (tws-string))
             ((Contract-localSymbol     con) :: (tws-string))
             ((Contract-tradingClass    con) :: (tws-string))
             ((Contract-includeExpired  con) :: (tws-boolean))
             ((Contract-secIdType       con) :: (tws-string))
             ((Contract-secId           con) :: (tws-string))))))

(: matching-symbols-msg (-> RequestId String Msg))
(define (matching-symbols-msg rid pattern)
  (message (bit-string->bytes
            (bit-string
             ((msg-id REQ-MATCHING-SYMBOLS) :: (tws-integer))
              (rid                          :: (tws-request-id))
              (pattern                      :: (tws-string))))))

(: cancel-historical-bar-data (-> TickerId Msg))
(define (cancel-historical-bar-data tid)
  (define VERSION 1)
  (message (bit-string->bytes
            (bit-string
             [(msg-id CANCEL-HISTORICAL-DATA) :: (tws-integer)]
             [VERSION :: (tws-integer)]
             [tid     :: (tws-ticker-id)]))))

(: historical-bar-data (-> RequestId Contract Date Duration Bar-Size Historical-Type Boolean Date-Format Boolean String Msg))
(define (historical-bar-data rid c edate dur bs htyp rth dform upd chart)
  (define VERSION 6)
  (message (bit-string->bytes
            (bit-string
             ((msg-id REQ-HISTORICAL-DATA)               :: (tws-integer))
             ;;(version :: (tws-integer))
             (rid                                       :: (tws-request-id))
             ((Contract-conId c)                        :: (tws-integer))
             ((Contract-symbol c)                       :: (tws-string))
             ((Contract-secType c)                      :: (tws-security-type))
             ((Contract-lastTradeDateOrContractMonth c) :: (tws-string))
             ((Contract-strike c)                       :: (tws-float))
             ((Contract-right c)                        :: (tws-string))
             ((Contract-multiplier c)                   :: (tws-string))
             ((Contract-exchange c)                     :: (tws-string))
             ((Contract-primaryExchange c)              :: (tws-string))
             ((Contract-currency c)                     :: (tws-string))
             ((Contract-localSymbol c)                  :: (tws-string))
             ((Contract-tradingClass c)                 :: (tws-string))
             ((Contract-includeExpired c)               :: (tws-boolean))
             (edate                                     :: (tws-date))
             (bs                                        :: (tws-bar-size))
             (dur                                       :: (tws-duration))
             (rth                                       :: (tws-boolean))
             (htyp                                      :: (tws-historical-type))
             (dform                                     :: (tws-date-format))
;; WTF?      (1                                         :: (tws-integer)) ;; FIXME Combolegs
             (upd                                       :: (tws-boolean))
             (""                                        :: (tws-string))  ;; NOT USED 
             ))))

(: tick-by-tick-data (-> RequestId Contract TickByTickType Integer Boolean Msg))
(define (tick-by-tick-data rid c t-type cnt ign-size?)
  (message (bit-string->bytes
            (bit-string
             ((msg-id REQ-TICK-BY-TICK-DATA)            :: (tws-integer))
             (rid                                       :: (tws-request-id))
             ((Contract-conId c)                        :: (tws-integer))
             ((Contract-symbol c)                       :: (tws-string))
             ((Contract-secType c)                      :: (tws-security-type))
             ((Contract-lastTradeDateOrContractMonth c) :: (tws-string))
             ((Contract-strike c)                       :: (tws-float))
             ((Contract-right c)                        :: (tws-string))
             ((Contract-multiplier c)                   :: (tws-string))
             ((Contract-exchange c)                     :: (tws-string))
             ((Contract-primaryExchange c)              :: (tws-string))
             ((Contract-currency c)                     :: (tws-string))
             ((Contract-localSymbol c)                  :: (tws-string))
             ((Contract-tradingClass c)                 :: (tws-string))
;;             ("AllLast"                                 :: (tws-string))
             (t-type                                    :: (tws-tick-by-tick-type))
             (cnt                                       :: (tws-integer))
             (ign-size?                                 :: (tws-boolean))))))


(: executions-msg (-> RequestId ExecutionFilter Msg))
(define (executions-msg rid efilter)
  (match efilter
    ((ExecutionFilter cid acc time sym st exch side)      
     (message (bit-string->bytes
               (bit-string
                (rid     :: (tws-request-id))
                (cid     :: (tws-integer))
                (acc     :: (tws-string))
                (time    :: (tws-string))
                (sym     :: (tws-string))
                (st      :: (tws-security-type))
                (exch    :: (tws-string))
                (side    :: (tws-symbol))))))))
