#lang typed/racket/no-check

(provide:
 [gen-rid (-> RequestId)]
 [gen-tid (-> TickerId)])

(require
 typed/racket/unit 
 (only-in "../data/field.rkt"
          RequestId request-id
          TickerId
          ticker-id)
 (only-in "gen-tx-id-sig.rkt"
          gen-tx-id^)
 (only-in "gen-tx-rand-unit.rkt"
          gen-rand-tx@))

(define-values/invoke-unit gen-rand-tx@
  (import)
  (export gen-tx-id^))
